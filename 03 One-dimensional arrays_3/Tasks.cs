﻿namespace OneDimArrays3
{
    public static class Tasks
    {
        public static (int, int) Task1(int[] array)
        {
            int sum = 0, number = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] != 0)
                {
                    number++;
                    sum += array[i];
                }
            }

            return (sum, number);
        }

        public static double Task2(int[] array, int a)
        {
            double result = 0;
            double number = 0, sum = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > a)
                {                    
                    sum += array[i];
                    number++;

                    result = sum / number;
                }
            }

            return result;
        }

        public static int Task3(int[] array)
        {
            int result = 0;

            for (int i = 0; i < array.Length; i++)
            {
                result = array.Max() - array.Min();
            }

            return result;
        }
    }
}
