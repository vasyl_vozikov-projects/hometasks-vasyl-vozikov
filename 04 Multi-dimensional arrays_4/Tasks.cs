﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multidimensional4Tasks
{
    public static class Tasks
    {
        public static int Task1(int[,] array)
        {
            int product = 1;

            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        product *= array[i, j];
                    }
                }
            }

            return product;
        }

        public static double Task2(int[,] array)
        {
            double maxValue = 0;

            for (int i = 0; i < array.GetLength(0); i++)
            {
                double sum = 0;
                double count = 0;
                double average = 0;

                for (int j = 0; j < array.GetLength(1); j++)
                {
                    sum += array[i,j];
                    count++;
                    average = sum / count;                    
                }

                if (average > maxValue)
                {
                    maxValue = average;
                }
            }
            return maxValue;
        }

        public static int[] Task3(int[,] array)
        {
            int rows = array.GetUpperBound(0) + 1;
            int columns = array.Length / rows;

            int[] sumarray = new int[columns];

            for (int i = 0; i < columns; i++)
            {
                sumarray[i] = 0;

                for (int j = 0; j < rows; j++)
                {
                    sumarray[i] += array[j, i];
                }
            }
            return sumarray;
        }
    }
}
