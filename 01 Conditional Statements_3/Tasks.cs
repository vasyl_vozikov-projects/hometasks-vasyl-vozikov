﻿namespace ConditionStatements3
{
    public static class Tasks
    {
        public static (int a, int b, int c) Task1(int a, int b, int c)
        {            
            // a < b < c
            if (a < b && a < c && b < c)
            {
                a = a;
                b = b;
                c = c;
            }
            // a < c < b
            if (a < b && a < c && c < b)
            {
                a = a;
                b = b;
                c = b;
            }
            // b < c < a
            if (b < a && b < c && c < a)
            {
                c = a;
                b = b;
                a = b;
            }
            // b < a < c
            if (b < a && b < c && a < c)
            {
                b = b;
                c = c;
                a = b;
            }
            // c < a < b
            if (c < a && c < b && a < b)
            {
                b = b;
                a = c;
                c = b;
            }
            // c < b < a		
            if (c < a && c < b && b < a)
            {
                b = b;
                (a, c) = (c, a);
            }

            return (a, b, c);            
        }
        

        public static (int a, int b, int c) Task2( int a,  int b, int c )
        {
            // a < b < c
            if (a < b && a < c && b < c)
            {
                a = 2 * a;
                b = 2 * b;
                c = 2 * c;
            }
            else
            {
                a = -1 * a;
                b = -1 * b;
                c = -1 * c;
            }

            return  (a, b, c);
        }
        
        public static (int a, int b, int c) Task3(int a,  int b, int c)
        {
            // a < b < c
            if (a < b && a < c && b < c)
            {
                a = 2 * a;
                b = 2 * b;
                c = 2 * c;
            }
            else
            { 
            // a > b > c
            if (a > b && a > c && b > c)
            {
                a = 2 * a;
                b = 2 * b;
                c = 2 * c;
            }
            else
            {
                a = -1 * a;
                b = -1 * b;
                c = -1 * c;
            }
            }

            return (a, b, c);
        }
    }  
}

