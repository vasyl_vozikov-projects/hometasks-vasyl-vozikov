using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CustomArray
{
    public class CustomArray<T> : IEnumerable<T>
    {
        public int First
        {
            get;    
            private set;
        }
        public int Last
        {
            get
            {
                return First + Length - 1;
            }
        }
        public int Length
        {
            get
            {
                return Array.Length;
            }
            private set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("value <= 0");
                }
            }
        }
        public T[] Array
        {
            get;
        }     
        public CustomArray(int first, int length)
        {
            First = first;
            Length = length;  
            Array = new T[length];
        }  
        public CustomArray(int first, IEnumerable<T> list)
        {
            if (list == null)
            {
                throw new NullReferenceException("Null");
            }
            if ((list.ToArray()).Length <= 0)
            {
                throw new ArgumentException("Count <= 0");
            }
            First = first;
            Array = list.ToArray();
        }   
        public CustomArray(int first, params T[] list)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list), "Null");
            }
            if (list.Length <= 0)
            {
                throw new ArgumentException("Length <= 0");
            }
            this.First = first;
            this.Array = list;
        }     
        public T this[int item]
        {
            get
            {
                if ((item < First) || (item > Last))
                {
                    throw new ArgumentException("Out of range");
                }
                return Array[item - First];
            }
            set
            {
                if ((item < First) || (item > Last))
                {
                    throw new ArgumentException("Out of range");
                }
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value), "Null");
                }
                Array[item - First] = value;
            }
        }
        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)Array).GetEnumerator();   
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return Array.GetEnumerator();
        }
    }
}
