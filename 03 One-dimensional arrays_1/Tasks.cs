﻿namespace OneDimArrays1
{
    public static class Tasks
    {
        public static int Task1(int[] array, int a)
        {
            int result = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > a)
                {
                    result += array[i];
                }
            }

            return result;
        }

        public static int Task2(int[] array)
        {
            int result = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < 0)
                {
                    result += array[i];
                }
            }

            return result;
        }

        public static int Task3(int[] array)
        {
            int result = 0;

            int even = array[0];
            int neven = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] % 2 == 0)
                {
                    even = array[i];
                    neven = i;

                    if (i % 2 == 0)
                    {
                        result += even;
                    }
                }
                
            }

            return result;
        }
    }
}
