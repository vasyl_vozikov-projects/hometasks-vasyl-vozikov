﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InheritanceTask
{
    public class Company
    {
        private readonly Employee[] employees;
        public Company(params Employee[] employees)
        {
            this.employees = employees;
        }
        public void GiveEverybodyBonus(decimal companyBonus)
        {
            for (int i = 0; i < employees.Length; i++)
            {                
                employees[i].SetBonus(companyBonus);
            }
        }
        public decimal TotalToPay()
        {
            decimal totalToPay = 0;

            for (int i = 0; i < employees.Length; i++)
            {
                totalToPay += employees[i].ToPay();
            }
            return totalToPay;
        }
        public string NameMaxSalary()
        {
            int index = 0;

            for (int i = 1; i < employees.Length; i++)
            {
                if (employees[index].ToPay() < employees[i].ToPay())
                {
                    index = i;
                }
            }
            return employees[index].Name;
        }
    }
}