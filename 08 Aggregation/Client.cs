namespace Aggregation
{
    class Client
    {
        private readonly Deposit[] deposits;
        public Client()
        {
            deposits = new Deposit[10];
        }
        public bool AddDeposit(Deposit deposit)
        {
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] == null)
                {
                    deposits[i] = deposit;
                    return true;
                }                
            }
            return false;
        }
        public decimal TotalIncome()
        {
            decimal totalIncome = 0;
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] != null)
                {
                    totalIncome += deposits[i].Income();
                }
            }
            return totalIncome;
        }
        public decimal MaxIncome()
        {
            decimal MaxDep = deposits[0].Income();
            for (int i = 1; i < deposits.Length; i++)
            {
                if (deposits[i] != null && MaxDep < deposits[i].Income())
                {
                    MaxDep = deposits[i].Income();                    
                }
            }
            return MaxDep;
        }
        public decimal GetIncomeByNumber(int number)
        {            
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[number-1] != null)
                {
                    decimal IncomeByNumber = deposits[number - 1].Income();
                    return IncomeByNumber;
                }
            }
            return 0;
        }
    }
}