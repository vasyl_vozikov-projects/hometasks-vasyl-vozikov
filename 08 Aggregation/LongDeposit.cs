using System;

namespace Aggregation
{
    class LongDeposit : Deposit
    {
        public LongDeposit(decimal depositAmount, int depositPeriod) : base(depositAmount, depositPeriod)
        {
        }
        public override decimal Income()
        {
            decimal IncomePerMonth = 0;
            decimal Sum = Amount;
            if (Period > 6)
            {
                for (int i = 6; i < Period; i++)
                {
                    IncomePerMonth = Sum * 0.15m;
                    Sum += IncomePerMonth;
                }
            }
            return Sum - Amount;
        }
    }
}