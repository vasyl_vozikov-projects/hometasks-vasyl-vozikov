using System;

namespace Aggregation
{
    class SpecialDeposit : Deposit
    {
        public SpecialDeposit(decimal depositAmount, int depositPeriod) : base(depositAmount, depositPeriod)
        {
        }
        public override decimal Income()
        {
            decimal IncomePerMonth = 0;
            decimal Sum = Amount;
            for (int i = 1; i <= Period; i++)
            {
                IncomePerMonth = Sum * i / 100;
                Sum += IncomePerMonth;
            }
            return Sum - Amount;
        }
    }
}