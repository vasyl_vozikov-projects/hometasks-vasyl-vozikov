namespace Loop1Tasks
{
    public static class Tasks
    {
        public static int Task1(int n, int m)
        {
            int sum = 0;

            for ( ; n <= m; n++)
            {
                if (n % 2 != 0) continue;
                sum += n;
            }

            return sum;

        }

        public static int Task2(int n, int m)
        {
            int sum = 0;

            for ( ; n <= m; n++)
            {
                if (n % 2 == 0) continue;
                sum += n;
            }

            return sum;
        }

        public static int Task3(int n)
        {
            int sum = 0;
            int m;

            while(n > 0)
            {
                m = n % 10;

                sum += m;

                n = n / 10;

            }

            return sum;
        }
    }
}
