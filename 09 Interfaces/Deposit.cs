﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    abstract class Deposit : IComparable<Deposit>
    { 
        public decimal Amount { get; }
        public int Period { get; }
        public Deposit(decimal depositAmount, int depositPeriod)
        {
            Amount = depositAmount;
            Period = depositPeriod;
        }
        public abstract decimal Income();
        public int CompareTo(Deposit deposit)
        {
            decimal currentSum = Amount + Income();
            decimal depositSum = deposit.Amount + deposit.Income();

            return currentSum.CompareTo(depositSum);
        }
        public override bool Equals(object obj)
        {
            if (obj != null)
            {
                Deposit deposit = obj as Deposit;
                if (!(deposit is null) && (Amount == deposit.Amount) && (Period == deposit.Period) && (Income() == deposit.Income()))
                {
                    return true;
                }
            }
            return false;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(Amount, Period);
        }
        public static bool operator > (Deposit operand1, Deposit operand2)
        {
            if (operand1 != null && operand1.CompareTo(operand2) == 1)
            {
                return true;
            }
            return false;
        }
        public static bool operator >= (Deposit operand1, Deposit operand2)
        {
            if (operand1 != null && operand1.CompareTo(operand2) >= 0)
            {
                return true;
            }
            return false;
        }
        public static bool operator < (Deposit operand1, Deposit operand2)
        {
            if (operand1 != null && operand1.CompareTo(operand2) == -1)
            {
                return true;
            }
            return false;
        }
        public static bool operator <= (Deposit operand1, Deposit operand2)
        {
            if (operand1 != null && operand1.CompareTo(operand2) <= 0)
            {
                return true;
            }
            return false;
        }
        public static bool operator == (Deposit operand1, Deposit operand2)
        {
            if (operand1 is null && operand2 is null)
            {
                return true;
            }
            else if (!(operand1 is null))
            {
                return operand1.Equals(operand2);
            }
            return false;
        }
        public static bool operator != (Deposit operand1, Deposit operand2)
        {
            if (operand1 is null && operand2 is null)
            {
                return false;
            }
            else if (!(operand1 is null))
            {
                return !operand1.Equals(operand2);
            }
            return false;
        }
    }
}
