﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    class SpecialDeposit : Deposit, IProlongable
    {
        public SpecialDeposit(decimal depositAmount, int depositPeriod) : base(depositAmount, depositPeriod)
        {
        }
        public override decimal Income()
        {
            decimal IncomePerMonth = 0;
            decimal Sum = Amount;
            for (int i = 1; i <= Period; i++)
            {
                IncomePerMonth = Sum * i / 100;
                Sum += IncomePerMonth;
            }
            return Sum - Amount;
        }
        public bool CanToProlong()
        {
            if (Amount > 1000)
            {
                return true;
            }
            return false;
        }
    }
}
