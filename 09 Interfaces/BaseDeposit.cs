﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    class BaseDeposit : Deposit
    {
        public BaseDeposit(decimal depositAmount, int depositPeriod) : base(depositAmount, depositPeriod)
        {
        }
        public override decimal Income()
        {
            decimal IncomePerMonth = 0;
            decimal Sum = Amount;
            for (int i = 0; i < Period; i++)
            {
                IncomePerMonth = Sum * 0.05m;
                Sum += Math.Round(IncomePerMonth, 2);
            }
            return Sum - Amount;
        }
    }
}
