﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    class LongDeposit : Deposit, IProlongable
    {
        public LongDeposit(decimal depositAmount, int depositPeriod) : base(depositAmount, depositPeriod)
        {
        }
        public override decimal Income()
        {
            decimal IncomePerMonth = 0;
            decimal Sum = Amount;
            if (Period > 6)
            {
                for (int i = 6; i < Period; i++)
                {
                    IncomePerMonth = Sum * 0.15m;
                    Sum += IncomePerMonth;
                }
            }
            return Sum - Amount;
        }
        public bool CanToProlong()
        {
            if (Period <= 36)
            {
                return true;
            }
            return false;
        }
    }
}
