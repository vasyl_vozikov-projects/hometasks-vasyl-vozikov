﻿namespace OneDimArrays4
{
    public static class Tasks
    {
        public static int Task1(int[] array, int a)
        {
            int n = 0;

            for ( ; n < array.Length; n++)
            {
                if (array[n] > a)
                    break;
            }

            for (int i = n + 1; i < array.Length; i++)
            {
                if (array[i] > a && array[i] < array[n])
                {
                    n = i;
                }                    
            }

            return array;
        }

        public static int[] Task2(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] % 2 == 0 && i % 2 == 0 || array[i] % 2 != 0 && i % 2 != 0)
                {
                    array[i] += i;
                }
            }

            return array;
        }

        public static int[] Task3(int[] array, int a)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > a)
                {
                    array[i] *= 2;
                }
                else
                {
                    array[i] = 0;
                }
            }

            return array;
        }
    }
}
