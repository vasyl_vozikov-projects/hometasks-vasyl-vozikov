# LINQ

Write LINQ queries using extension methods. Each task must be solved with
one LINQ query. Put LINQ expression in the return statement (Look at the template on the end of the task before solving the problem)

You need to solve the following five tasks:

### Task 1
The character **C** and a sequence of non-empty strings **stringList** are given.
Get a new sequence of strings with more than one character from the **stringList**, starting and
ending with C.

### Task 2
A sequence of non-empty strings **stringList** is given.
Get a sequence of ascending sorted integer values equal to the lengths of the strings included
in the **stringList** sequence.

### Task 3
A sequence of non-empty strings **stringList** is given.
Get a new sequence of strings, where each string consists of the first and last characters of the
corresponding string in the **stringList** sequence.

### Task 4
A positive integer **K** and a sequence of non-empty strings **stringList** are given. Strings of the
sequence contain only numbers and capital letters of the Latin alphabet.
Get from **stringList** all strings of length **K** ending in a digit and sort them in ascending order.

### Task 5
A sequence of positive integer values **integerList** is given.
Get sequence of string representations of only odd **integerList** values and sort in ascending
order.

## An example of solving a LINQ task
**Task** 

Given a sequence of non-empty strings **stringList**. Get a new 
string by concatenating all strings from **stringList**.

Consider that the values have already been set for **stringList**, and the
assignment of new values is an error.
Return the result of solving the problem via the function.

```
public static string LinqSample(IEnumerable<string> stringList)
{
        return stringList.Aggregate<string>((x, y) => x + y);
}
```
