﻿using System.Linq;

namespace Loop2Tasks
{
    public static class Tasks
    {
        public static int Task1(int n)
        {
            int sum = 0;
            int m;

            for ( ; n > 0; n = n/ 10)
            {
                if (n % 2 != 0) continue;

                m = n % 10;

                sum += m;
            }

            return sum;

        }

        public static int Task2(int n)
        {            
            int evensum = 0;
            int oddsum = 0;
            int m;

            for ( ; n > 0; n = n / 10)
            {
                m = n % 10;

                if (m % 2 == 0)
                {                
                evensum += m;
                }

                else
                {
                oddsum += m;
                }                
            }

            int max = Math.Max(evensum, oddsum);

            return max;
        }

        public static int Task3(int n)
        {
            int result = 0;

            for (int i = 1; i <= 9; i++)
            {
                int t = n;

                for ( ; t > 0; t /= 10)
                {
                    int digit = t % 10;

                    if (digit == i)
                    {
                        result *= 10;
                        result += digit;
                    }
                    
                }

            }
            return result;
        }
    }
}