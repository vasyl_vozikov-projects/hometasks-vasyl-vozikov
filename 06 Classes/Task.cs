using System;
using System.Collections.Generic;
using System.Linq;

namespace Class
{
    class Rectangle
    {
        private double sideA;
        private double sideB;

        public Rectangle(double sideA, double sideB)
        {
            this.sideA = sideA;
            this.sideB = sideB;
        }

        public Rectangle(double a)
        {
            this.sideA = a;
            this.sideB = 5;
        }

        public Rectangle()
        {
            this.sideA = 4;
            this.sideB = 3;
        }

        public double GetSideA()
        {
            return sideA;
        }

        public double GetSideB()
        {
            return sideB;
        }

        public double Area()
        {
            return sideA * sideB;
        }

        public double Perimeter()
        {
            return (sideA + sideB) * 2;
        }

        public bool IsSquare()
        {
            if (sideA == sideB)
            {
                return true;
            }
            return false;
        }

        public void ReplaceSides()
        {
            (sideA,sideB) = (sideB, sideA);
        }
    }

    class ArrayRectangles
    {
        private readonly Rectangle[] rectangle_array;

        public ArrayRectangles(int n)
        {
            this.rectangle_array = new Rectangle[n];
        }

        public bool AddRectangle(Rectangle rectangle)
        {
            for (int i = 0; i < rectangle_array.Length; i++)
            {
                if (rectangle_array[i] == null)
                {
                    rectangle_array[i] = rectangle;
                    return true;
                }
            }
            return false;
        }

        public int NumberMaxArea()
        {
            int IndexOfMaxRectangleArea = 0;
            double MaxsArea = 0;

            for (int i = 0; i < rectangle_array.Length; i++)
            {
                if (rectangle_array[i].Area() > MaxsArea)
                {
                    IndexOfMaxRectangleArea = i;
                    MaxsArea = rectangle_array[i].Area();
                }
            }
            return IndexOfMaxRectangleArea;
        }

        public int NumberMinPerimeter()
        {
            int IndexOfMinRectanglePerimeter = 0;
            double MinPerimeter = 0;

            for (int i = 0; i < rectangle_array.Length; i++)
            {
                if (rectangle_array[i].Perimeter() > MinPerimeter)
                {
                    IndexOfMinRectanglePerimeter = i;
                    MinPerimeter = rectangle_array[i].Area();
                }
            }
            return IndexOfMinRectanglePerimeter;
        }

        public int NumberSquare()
        {
            int NumberOfSquares = 0;

            for (int i = 0; i < rectangle_array.Length; i++)
            {
                if (rectangle_array[i].IsSquare())
                {
                    NumberOfSquares++;
                }
            }
            return NumberOfSquares;
        }
    }
}
