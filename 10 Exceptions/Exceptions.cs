﻿using System;

namespace Exceptions
{
    [Serializable]
    public class MatrixException : Exception
    {
        public MatrixException() { }
        public MatrixException(string message) : base(message) { }
        public MatrixException(string message, Exception innerException) : base(message, innerException) { }
        protected MatrixException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
    public class Matrix
    {
        public double[,] Array
        {
            get;
        }
        public int Rows
        {
            get;
        }
        public int Columns
        {
            get;
        }
        public Matrix(int rows, int columns)
        {            
            if (rows <= 0)
            {
                throw new ArgumentOutOfRangeException("rows", "ArgumentOutOfRangeException");
            }
            if (columns <= 0)
            {
                throw new ArgumentOutOfRangeException("columns", "ArgumentOutOfRangeException");
            }
            else
            {
                Array = new double[rows, columns];
                Rows = rows;
                Columns = columns;
            }
        }
        public Matrix(double[,] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }
            else
            {
                this.Array = array;
                this.Rows = array.GetLength(0);
                this.Columns = array.GetLength(1);
            }
        }
        public double this[int row, int column]
        {
            get
            {
                if (row < 0 || row >= this.Rows)
                {
                    throw new ArgumentException("rows");
                }
                if (column < 0 || column >= this.Columns)
                {
                    throw new ArgumentException("columns");
                }
                else
                {
                    return Array[row, column];
                }
            }
            set
            {
                if (row < 0 || row >= this.Rows)
                {
                    throw new ArgumentException("rows");
                }
                if (column < 0 || column >= this.Columns)
                {
                    throw new ArgumentException("columns");
                }
                else
                {
                    Array[row, column] = value;
                }
            }
        }
        public Matrix Add(Matrix matrix)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException("matrix");
            }
            else
            {
                if (this.Rows != matrix.Rows || this.Columns != matrix.Columns)
                {
                    throw new MatrixException();
                }
                else
                {
                    Matrix result = new Matrix(this.Rows, this.Columns);
                    for (int i = 0; i < this.Rows; i++)
                    {
                        for (int j = 0; j < this.Columns; j++)
                        {
                            result.Array[i, j] = matrix.Array[i, j] + this.Array[i, j];
                        }
                    }
                    return result;
                } 
            }
        }
        public Matrix Subtract(Matrix matrix)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException("matrix");
            }
            else
            {
                if (this.Rows != matrix.Rows || this.Columns != matrix.Columns)
                {
                    throw new MatrixException();
                }
                else
                {
                    Matrix result = new Matrix(this.Rows, this.Columns);
                    for (int i = 0; i < this.Rows; i++)
                    {
                        for (int j = 0; j < this.Columns; j++)
                        {
                            result.Array[i, j] = this.Array[i, j] - matrix.Array[i, j];
                        }
                    }
                    return result;
                }
            }
        }
        public Matrix Multiply(Matrix matrix)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException("matrix");
            }
            if (this.Columns != matrix.Rows)
            {
                throw new MatrixException();
            }
            double[,] result = new double[this.Rows, matrix.Columns];
            for (int i = 0; i < this.Rows; i++)
            {
                for (int j = 0; j < matrix.Columns; j++)
                {
                    result[i, j] = 0;
                    for (int k = 0; k < this.Columns; k++)
                    {
                        result[i, j] += this.Array[i, k] * matrix.Array[k, j];
                    }
                }
            }
            return new Matrix(result);     
        }
    }
}