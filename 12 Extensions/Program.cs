﻿using System;
using System.Collections.Generic;

namespace Extension
{
    public static class MyExtension
    {
        public static int SummaDigit(this int n)
        {
            int sum = 0;
            for (int i = 0; i < Math.Abs(n); n /= 10)
            {
                sum += Math.Abs(n % 10);
            }
            return sum;
        }
        public static ulong SummaWithReverse(this uint n)
        {
            uint temp = 0;
            uint result = n;
            for (uint i = 0; i < n; n /= 10)
            {
                temp *= 10;
                temp += n % 10;
            }
            return result + temp;
        }
        public static int CountNotLetter(this string str)
        {
            int count = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if ((str[i] >= 'a' && str[i] <= 'z') || (str[i] >= 'A' && str[i] <= 'Z'))
                {
                    continue;
                }
                else if (str[i] >= '0' && str[i] <= '9')
                {
                    count++;
                }
                else
                {
                    count++;
                }
            }
            return count;
        }
        public static bool IsDayOff(this DayOfWeek day)
        {
            return (day == DayOfWeek.Saturday || day == DayOfWeek.Sunday);
        }
        public static IEnumerable<int> EvenPositiveElements(this IEnumerable<int> numbers)
        {
            foreach (int number in numbers)
            {
                if (number > 0 && number % 2 == 0)
                {
                    yield return number;
                }
            }
        }
    }
}
