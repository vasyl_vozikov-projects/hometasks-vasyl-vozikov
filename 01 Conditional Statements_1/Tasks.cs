﻿namespace ConditionStatements1
{
    public static class Tasks
    {
        public static int Task1(int a)
        {     
            
        if(a > 0)
        {
            a = a + 1;

            return a;
        }
        else
        {
            return a;
        }

        }
        
        public static int Task2(int a)
        {           
        
        if(a > 0)
        {
            a = a + 1;
            
            return a;
        }        

        else
        {
            a = a - 2;

            return a;
        }

        }
        
        public static int Task3(int a)
        {            
        if(a > 0)
        {
            a = a + 1;

            return a;
        }
        else 
        {
            if(a < 0)
            {
                a = a - 2;

                return a;
            }
            else
            {
                a = a + 3;

                return a;
            }
        }

        }
    }
}

