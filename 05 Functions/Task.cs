﻿using System;

namespace Function
{
    public enum SortOrder { Ascending, Descending }
    public static class Function
    {
        public static bool IsSorted(int[] array, SortOrder order)
        {
            for (int i = 1; i < array.Length; i++)
                if (order == SortOrder.Ascending)
                {
                    if (array[i] < array[i - 1]) return false;
                }
                else if (array[i] > array[i - 1]) return false;
            return true;
        }

        public static void Transform(int[] array, SortOrder order)
        {
            if (IsSorted(array, order))
            {
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] += i;
                }
            }
        }

        public static double MultArithmeticElements(double a, double t, int n)
        {
            double result = 1;
            for (int i = 0; i < n; i++)
            {
                result *= a;
                a += t;
            }
            return result;
        }

        public static double SumGeometricElements(double a, double t, double alim)
        {
            double result = 0;
            for (double i = alim; i > 0; i--)
            {
                if (a < alim) break;

                result += a;
                a *= t;
            }
            return result;
        }
    }
}
