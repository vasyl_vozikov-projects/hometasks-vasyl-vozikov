﻿using System;

namespace LoopTasks
{
    public static class LoopTasks
    {

        public static int SumOfOddDigits(int n)
        {
            int sum = 0;
            while (n > 0)
            {
                if (n % 10 % 2 != 0)
                {
                    sum += n % 10;
                }

                n /= 10;
            }
            return sum;
        }

        public static int NumberOfUnitsInBinaryRecord(int n)
        {
            int sum = 0;

            while (n > 0)
            {
                if (n % 2 != 0)
                {
                    sum++;
                }

                n /= 2;
            }
            return sum;
        }

        public static int SumOfFirstNFibonacciNumbers(int n)
        {
            int sum = 0;
            int f0 = 0;
            int f1 = 1;
            int next;

            for (int i = 0; i < n; i++)
            {
                sum += f0;
                next = f0 + f1;
                f0 = f1;
                f1 = next;
            }

            return sum;
        }
    }
}